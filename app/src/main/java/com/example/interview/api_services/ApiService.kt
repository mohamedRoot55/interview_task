package com.example.interview.api_services

import android.util.Log
import com.example.interview.domain.entities.Station

class ApiService {
    private  val TAG = "ApiService"
    companion object {
        private var instance: ApiService? = null
        fun getInstance(): ApiService {
            if (instance == null) {
                instance = ApiService()
            }

            return instance!!
        }
    }

    fun fetchStationResponse(): List<Station> {


        val stations = mutableListOf<Station>()
        // Sixt, europcar, ADAC, Hertz
        // cairo stations - ismailia stations

        stations.add(0, Station(0, stationName = "Cairo Station", stationProvider = "Sixt"))
        stations.add(0, Station(0, stationName = "Ismailia Station", stationProvider = "europcar"))
        stations.add(0, Station(0, stationName = "nasr city Station", stationProvider = "ADAC"))
        stations.add(0, Station(0, stationName = "city 22 Station", stationProvider = "Hertz"))


        return stations
    }


    fun fetchStationResponseByKeyword(keyword: String): List<Station> {


        val stations = mutableListOf<Station>()
        stations.add(0, Station(0, stationName = "Cairo Station", stationProvider = "Sixt"))
        stations.add(0, Station(0, stationName = "Ismailia Station", stationProvider = "europcar"))
        stations.add(0, Station(0, stationName = "nasr city Station", stationProvider = "ADAC"))
        stations.add(0, Station(0, stationName = "city 22 Station", stationProvider = "Hertz"))

        // contain o(n) // for loop o(n)

       val filtredList =   stations.filter { s ->
            s.stationName!!.contains(keyword) || s.stationProvider!!.contains(
                keyword
            )
        }

        Log.d(TAG, "fetchStationResponseByKeyword: $filtredList")

        return filtredList
    }

}