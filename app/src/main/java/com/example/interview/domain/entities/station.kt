package com.example.interview.domain.entities

data class Station(
    val stationId: Int? = 0,
    val stationProvider: String? = "",
    val stationName: String? = ""
)