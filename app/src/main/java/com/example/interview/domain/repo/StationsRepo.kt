package com.example.interview.domain.repo

import com.example.interview.api_services.ApiService
import com.example.interview.domain.entities.Station

class StationsRepo {
    var apiService: ApiService = ApiService.getInstance()

    fun getStations(): List<Station> {
        return apiService.fetchStationResponse()
    }
    fun searchInStations(keyword : String): List<Station>{
        return apiService.fetchStationResponseByKeyword(keyword)
    }

    companion object{
        private var instance : StationsRepo? = null
        fun getInstance() : StationsRepo {
            if(instance == null){
                instance = StationsRepo()
            }

            return instance!!
        }
    }
}