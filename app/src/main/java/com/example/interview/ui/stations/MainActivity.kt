package com.example.interview.ui.stations

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interview.databinding.ActivityMainBinding
import com.example.interview.ui.stations.adapter.StationsAdapter

class MainActivity : AppCompatActivity() {
    private val adapter: StationsAdapter by lazy {
        StationsAdapter()
    }



    lateinit var binding: ActivityMainBinding

    val model: StationsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        initViews()
        getStations()
        observe()

        setContentView(binding.root)
    }

    private fun observe() {
        model.stations.observe(this) {
            adapter.setDate(it)
            binding.stationRv.adapter = adapter
        }
    }

    private fun getStations() {
        model.getStation()
    }

    private fun initViews() {
        binding.stationRv.layoutManager = LinearLayoutManager(this)
        binding.searchEt.addTextChangedListener {
            model.searchInStations(it.toString())
        }
    }
}