package com.example.interview.ui.stations

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.interview.domain.entities.Station
import com.example.interview.domain.repo.StationsRepo

class StationsViewModel : ViewModel() {
    var stations = MutableLiveData<List<Station>>()

    val repo = StationsRepo.getInstance()


    fun getStation() {
        val list = repo.getStations()
        stations.value = list
    }
    fun searchInStations(keyword : String){
        val list = repo.searchInStations(keyword)
        stations.value = list
    }


}