package com.example.interview.ui.stations.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.interview.domain.entities.Station
import com.example.interview.databinding.StationViewHolderBinding

class StationsAdapter  : RecyclerView.Adapter<StationsAdapter.StationsViewHolder>() {
  var stations = emptyList<Station>()

    inner class StationsViewHolder(val binding : StationViewHolderBinding ) : RecyclerView.ViewHolder(binding.root){
        fun bind(station : Station) {
            binding.apply {
                stationName.text = station.stationName
                providerName.text = station.stationProvider
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDate(stations : List<Station>) {
        this.stations = stations
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationsViewHolder {
        val binding = StationViewHolderBinding.inflate(LayoutInflater.from(parent.context))
        return  StationsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StationsViewHolder, position: Int) {
       holder.bind(station = stations[position])
    }

    override fun getItemCount(): Int {
        return  stations.size
    }

}